import sys
from cx_Freeze import setup, Executable

# Dependecies are automatically detected,
# but it might need fine tuniing.

build_exe_options = {"packages": ["os"], "includes": ["tkinter"]}

# GUI applications require a diferent base on Windows
# (the defaut is for a console applications).

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name = "Conversor de porcentagem",
    version = "0.1",
    description = "Conversor de porcentagem_lida em paginas",
    options = {"build_exe": build_exe_options},
    executables = [Executable("tela.py", base=base)]

)
