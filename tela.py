import PySimpleGUI as bm
#import Tkinter
class TelaPython:
    def __init__(self):
        # Layout
        layout = [
            [bm.Text('total_de_paginas',size=(20,0)),bm.Input(size=(10,0),key='total_de_paginas')],
            [bm.Text('porcentagem_lida',size=(20,0)),bm.Input(size=(10,0),key='porcentagem_lida')],
            [bm.Button('CALCULAR')],
            [bm.Output(size=(30,20))]
        ]
        # Janela
        self.janela = bm.Window("CONVERSOR").layout(layout)
        # Extrair os dados da tela


    def Iniciar(self):
        while True:
            self.button, self.values = self.janela.Read()
            total_de_paginas = int(self.values['total_de_paginas'])
            porcentagem_lida = int(self.values['porcentagem_lida'])
            print(f'total_de_paginas: {total_de_paginas}')
            print(f'porcentagem_lida: {porcentagem_lida}')

            paginas_lidas = (total_de_paginas / 100) * porcentagem_lida
            print('Li {:.0f} paginas'.format(paginas_lidas))

tela = TelaPython()
tela.Iniciar()
